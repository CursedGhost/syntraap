﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elementary9
{
	class Program
	{
		static void Main(string[] args)
		{
			const int min = 0, max = 100;
			Console.WriteLine("Guess the number between {0} and {1} (inclusive).", min, max);

			// Setting up secret number between 0 and 100 inclusive
			Random rnd = new Random();
			int n = rnd.Next(min, max + 1);

			// Some variables that keep track of the game
			int tries = 0, lastGuess = -1, guess = 0;
			do {
				// Get user input
				Console.Write("> ");
				string input = Console.ReadLine();
				// Validate to a number
				if (!int.TryParse(input, out guess)) {
					Console.WriteLine("This is not a number!");
					continue;
				}
				// Must be in guessing range
				if (guess < 0 || guess > max) {
					Console.WriteLine("Please enter a number between {0} and {1}!", min, max);
					continue;
				}
				// Filter out repeat guesses
				if (guess == lastGuess) {
					Console.WriteLine("You already guessed this number!");
					continue;
				}
				// Increment tries and update lastGuess
				++tries;
				lastGuess = guess;
				// Feedback the number guessed was incorrect
				if (guess != n) {
					Console.WriteLine("The number is {0} than {1}!", guess > n ? "smaller" : "larger", guess);
					continue;
				}
			}
			while (guess != n);
			// Ending line and quit
			Console.WriteLine("Congratulations!\nYou guessed the number in {0} tries!", tries);
		}
	}
}
