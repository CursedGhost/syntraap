﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elementary5
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.Write("Enter a number: ");
			int n = int.Parse(Console.ReadLine());
			int result = 0;
			for (int i = 1; i <= n; ++i) {
				if ((i % 3) == 0 || (i % 5) == 0) {
					result += i;
				}
			}
			Console.WriteLine("Sum of numbers 1 to {0} that divide 3 or 5 is {1}.", n, result);
		}
	}
}
