﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elementary11
{
	class Program
	{
		static void Main(string[] args)
		{
			double result = 0.0;
			for (int k = 1; k <= 1000000; ++k) {
				// Subtract if even, add if odd
				double numerator = ((k % 2) == 1) ? 1.0 : -1.0;
				result += numerator / (double)(2 * k + 1);
			}
			result *= 4.0;
			Console.WriteLine("Result is {0}.", result);
		}
	}
}
