﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elementary8
{
	class Program
	{
		static void Main(string[] args)
		{
			for (int n = 2; n <= 2000; ++n) {
				if (IsPrime(n)) {
					Console.WriteLine(n);
				}
			}
		}

		static bool IsPrime(int n)
		{
			int max = (int)Math.Sqrt((double)n);
			for (int i = 2; i <= max; ++i) {
				if ((n % i)==0) {
					return false;
				}
			}
			return true;
		}
	}
}
