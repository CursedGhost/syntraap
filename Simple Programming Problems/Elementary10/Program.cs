﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elementary10
{
	class Program
	{
		static void Main(string[] args)
		{
			for (int year = 2015, leapYears = 0; leapYears < 20; ++year) {
				if ((year % 4) == 0 && (year % 200) != 0) {
					Console.WriteLine("The year {0} will be a leap year.", year);
					++leapYears;
				}
			}
		}
	}
}
