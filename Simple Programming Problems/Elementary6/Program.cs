﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elementary6
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.Write("Enter a number: ");
			int n;
			if (!int.TryParse(Console.ReadLine(), out n)) {
				Console.WriteLine("Please enter a number!");
				return;
			}

			Console.Write("Enter * or +: ");
			string opS = Console.ReadLine();
			if (opS.Length != 1 || !(opS[0] == '+' || opS[0] == '*')) {
				Console.WriteLine("Please enter an operator!");
				return;
			}
			char op = opS[0];

			int result;

			if (op == '+') {
				result = 0;
				for (int i = 1; i <= n; ++i) {
					result += i;
				}
			}
			else {
				result = 1;
				for (int i = 1; i <= n; ++i ) {
					result *= i;
				}
			}

			Console.WriteLine("The {0} of 1 to {1} is {2}.", op == '+' ? "sum" : "product", n, result);
		}
	}
}
