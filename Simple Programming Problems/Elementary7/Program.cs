﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elementary7
{
	class Program
	{
		static void Main(string[] args)
		{
			int n = 12;

			// Write the header
			Console.Write("   |");
			for(int i = 1; i <= n; ++i) {
				Console.Write("{0}|", i);
			}
			Console.Write("\n---|");
			for(int i = 1; i <= n; ++i) {
				Console.Write("----|");
			}
			Console.WriteLine();

			// Write each row
			for (int j = 1; j <= n; ++j) {
				// The premeable
				Console.Write("{0}|", j);
				// Followed by each column
				for (int i = 1; i <= n; ++i) {
					Console.Write("{0}|", j * i);
				}
				Console.WriteLine();
			}
		}
	}
}
