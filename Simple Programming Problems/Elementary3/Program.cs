﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elementary3
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.Write("Enter your name: ");
			string name = Console.ReadLine();
			if (name == "Alice" || name == "Bob") {
				Console.WriteLine("Hello, " + name + "!");
			}
		}
	}
}
