﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LolCalc
{
    public partial class Form3 : Form
    {
        double storedValue = 0;
        char storedOperator = '\0';
        bool storedClear = false;

        public Form3()
        {
            InitializeComponent();
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            lblDisplay.Text = "";
            textDisplay.Text = "0";
        }

        private void btnDigit_Click(object sender, EventArgs e)
        {
            // Clear the display as requested
            if (storedClear) {
                textDisplay.Text = "0";
                storedClear = false;
            }

            // Append to the displayed text
            string digit = ((Button)sender).Text;
            string text = textDisplay.Text + digit;

            // Collapse any leading zeros into a single zero
            // Ugly as fuck... Temporarily remove the sign so TrimStart works...
            string text_wo_sign = text[0] == '-' ? text.Substring(1) : text;
            if (text_wo_sign[0] == '0') {
                text_wo_sign = text_wo_sign.TrimStart('0');
                if (text_wo_sign.Length == 0 || text_wo_sign[0] == '.') {
                    text_wo_sign = "0" + text_wo_sign;
                }
            }
            text = text[0] == '-' ? "-" + text_wo_sign : text_wo_sign;

            // Check if still a valid number
            double value;
            if (double.TryParse(text, out value)) {
                textDisplay.Text = text;
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            textDisplay.Text = "0";
            storedOperator = '\0';
            storedValue = 0;
            lblDisplay.Text = "";
        }

        private void btnNegate_Click(object sender, EventArgs e)
        {
            string text = textDisplay.Text;
            if (text.Length > 0) {
                if (text[0] == '-') {
                    text = text.Substring(1);
                }
                else {
                    text = "-" + text;
                }
                textDisplay.Text = text;
            }
        }

        private void btnBackspace_Click(object sender, EventArgs e)
        {
            string text = textDisplay.Text;
            if (text.Length > 1) {
                // Remove the last character from the string
                text = text.Substring(0, text.Length - 1);
                // Fixup string if all that remains is a negation sign
                if (text == "-") {
                    text = "0";
                }
            }
            else {
                text = "0";
            }
            textDisplay.Text = text;
        }

        private void btnOperator_Click(object sender, EventArgs e)
        {
            // Apply the previously stored operator
            double value = double.Parse(textDisplay.Text);
            if (storedOperator == '+') {
                storedValue += value;
            }
            else if (storedOperator == '-') {
                storedValue -= value;
            }
            else if (storedOperator == '×') {
                storedValue *= value;
            }
            else if (storedOperator == '÷') {
                storedValue /= value;
            }
            else if (!storedClear) {
                storedValue = value;
            }

            // Get the next operator
            char op = ((Button)sender).Text[0];
            storedOperator = op;

            // Clear display when typing next
            storedClear = true;

            // Format label
            if (op == '=') {
                lblDisplay.Text = "= " + storedValue.ToString();
            }
            else {
                lblDisplay.Text = storedValue.ToString() + " " + op;
            }
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys KeyCode)
        {
            switch(KeyCode) {
            case Keys.NumPad0: btnDigit_Click(btnDigit0, null); break;
            case Keys.NumPad1: btnDigit_Click(btnDigit1, null); break;
            case Keys.NumPad2: btnDigit_Click(btnDigit2, null); break;
            case Keys.NumPad3: btnDigit_Click(btnDigit3, null); break;
            case Keys.NumPad4: btnDigit_Click(btnDigit4, null); break;
            case Keys.NumPad5: btnDigit_Click(btnDigit5, null); break;
            case Keys.NumPad6: btnDigit_Click(btnDigit6, null); break;
            case Keys.NumPad7: btnDigit_Click(btnDigit7, null); break;
            case Keys.NumPad8: btnDigit_Click(btnDigit8, null); break;
            case Keys.NumPad9: btnDigit_Click(btnDigit9, null); break;
            case Keys.Decimal: btnDigit_Click(btnDecimal, null); break;
            case Keys.Enter: btnOperator_Click(btnCalc, null); break;
            case Keys.Add: btnOperator_Click(btnOpAdd, null); break;
            case Keys.Subtract: btnOperator_Click(btnOpMin, null); break;
            case Keys.Multiply: btnOperator_Click(btnOpMul, null); break;
            case Keys.Divide: btnOperator_Click(btnOpDiv, null); break;
            case Keys.Back: btnBackspace_Click(btnBackspace, null); break;
            default: return false;
            }
            return true;
        }
    }
}
