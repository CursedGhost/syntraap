﻿namespace LolCalc
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form3));
            this.btnOpMin = new System.Windows.Forms.Button();
            this.btnOpMul = new System.Windows.Forms.Button();
            this.btnOpDiv = new System.Windows.Forms.Button();
            this.btnCalc = new System.Windows.Forms.Button();
            this.btnOpAdd = new System.Windows.Forms.Button();
            this.btnDigit1 = new System.Windows.Forms.Button();
            this.btnDigit2 = new System.Windows.Forms.Button();
            this.btnDigit3 = new System.Windows.Forms.Button();
            this.btnDigit6 = new System.Windows.Forms.Button();
            this.btnDigit5 = new System.Windows.Forms.Button();
            this.btnDigit4 = new System.Windows.Forms.Button();
            this.btnDigit9 = new System.Windows.Forms.Button();
            this.btnDigit8 = new System.Windows.Forms.Button();
            this.btnDigit7 = new System.Windows.Forms.Button();
            this.btnDigit0 = new System.Windows.Forms.Button();
            this.btnNegate = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnBackspace = new System.Windows.Forms.Button();
            this.btnDecimal = new System.Windows.Forms.Button();
            this.textDisplay = new System.Windows.Forms.TextBox();
            this.lblDisplay = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnOpMin
            // 
            this.btnOpMin.Location = new System.Drawing.Point(126, 152);
            this.btnOpMin.Name = "btnOpMin";
            this.btnOpMin.Size = new System.Drawing.Size(32, 32);
            this.btnOpMin.TabIndex = 4;
            this.btnOpMin.Text = "-";
            this.btnOpMin.UseVisualStyleBackColor = true;
            this.btnOpMin.Click += new System.EventHandler(this.btnOperator_Click);
            // 
            // btnOpMul
            // 
            this.btnOpMul.Location = new System.Drawing.Point(126, 114);
            this.btnOpMul.Name = "btnOpMul";
            this.btnOpMul.Size = new System.Drawing.Size(32, 32);
            this.btnOpMul.TabIndex = 5;
            this.btnOpMul.Text = "×";
            this.btnOpMul.UseVisualStyleBackColor = true;
            this.btnOpMul.Click += new System.EventHandler(this.btnOperator_Click);
            // 
            // btnOpDiv
            // 
            this.btnOpDiv.Location = new System.Drawing.Point(126, 76);
            this.btnOpDiv.Name = "btnOpDiv";
            this.btnOpDiv.Size = new System.Drawing.Size(32, 32);
            this.btnOpDiv.TabIndex = 6;
            this.btnOpDiv.Text = "÷";
            this.btnOpDiv.UseVisualStyleBackColor = true;
            this.btnOpDiv.Click += new System.EventHandler(this.btnOperator_Click);
            // 
            // btnCalc
            // 
            this.btnCalc.Location = new System.Drawing.Point(126, 228);
            this.btnCalc.Name = "btnCalc";
            this.btnCalc.Size = new System.Drawing.Size(32, 32);
            this.btnCalc.TabIndex = 7;
            this.btnCalc.Text = "=";
            this.btnCalc.UseVisualStyleBackColor = true;
            this.btnCalc.Click += new System.EventHandler(this.btnOperator_Click);
            // 
            // btnOpAdd
            // 
            this.btnOpAdd.Location = new System.Drawing.Point(126, 190);
            this.btnOpAdd.Name = "btnOpAdd";
            this.btnOpAdd.Size = new System.Drawing.Size(32, 32);
            this.btnOpAdd.TabIndex = 3;
            this.btnOpAdd.Text = "+";
            this.btnOpAdd.UseVisualStyleBackColor = true;
            this.btnOpAdd.Click += new System.EventHandler(this.btnOperator_Click);
            // 
            // btnDigit1
            // 
            this.btnDigit1.Location = new System.Drawing.Point(12, 190);
            this.btnDigit1.Name = "btnDigit1";
            this.btnDigit1.Size = new System.Drawing.Size(32, 32);
            this.btnDigit1.TabIndex = 8;
            this.btnDigit1.Text = "1";
            this.btnDigit1.UseVisualStyleBackColor = true;
            this.btnDigit1.Click += new System.EventHandler(this.btnDigit_Click);
            // 
            // btnDigit2
            // 
            this.btnDigit2.Location = new System.Drawing.Point(50, 190);
            this.btnDigit2.Name = "btnDigit2";
            this.btnDigit2.Size = new System.Drawing.Size(32, 32);
            this.btnDigit2.TabIndex = 9;
            this.btnDigit2.Text = "2";
            this.btnDigit2.UseVisualStyleBackColor = true;
            this.btnDigit2.Click += new System.EventHandler(this.btnDigit_Click);
            // 
            // btnDigit3
            // 
            this.btnDigit3.Location = new System.Drawing.Point(88, 190);
            this.btnDigit3.Name = "btnDigit3";
            this.btnDigit3.Size = new System.Drawing.Size(32, 32);
            this.btnDigit3.TabIndex = 10;
            this.btnDigit3.Text = "3";
            this.btnDigit3.UseVisualStyleBackColor = true;
            this.btnDigit3.Click += new System.EventHandler(this.btnDigit_Click);
            // 
            // btnDigit6
            // 
            this.btnDigit6.Location = new System.Drawing.Point(88, 152);
            this.btnDigit6.Name = "btnDigit6";
            this.btnDigit6.Size = new System.Drawing.Size(32, 32);
            this.btnDigit6.TabIndex = 13;
            this.btnDigit6.Text = "6";
            this.btnDigit6.UseVisualStyleBackColor = true;
            this.btnDigit6.Click += new System.EventHandler(this.btnDigit_Click);
            // 
            // btnDigit5
            // 
            this.btnDigit5.Location = new System.Drawing.Point(50, 152);
            this.btnDigit5.Name = "btnDigit5";
            this.btnDigit5.Size = new System.Drawing.Size(32, 32);
            this.btnDigit5.TabIndex = 12;
            this.btnDigit5.Text = "5";
            this.btnDigit5.UseVisualStyleBackColor = true;
            this.btnDigit5.Click += new System.EventHandler(this.btnDigit_Click);
            // 
            // btnDigit4
            // 
            this.btnDigit4.Location = new System.Drawing.Point(12, 152);
            this.btnDigit4.Name = "btnDigit4";
            this.btnDigit4.Size = new System.Drawing.Size(32, 32);
            this.btnDigit4.TabIndex = 11;
            this.btnDigit4.Text = "4";
            this.btnDigit4.UseVisualStyleBackColor = true;
            this.btnDigit4.Click += new System.EventHandler(this.btnDigit_Click);
            // 
            // btnDigit9
            // 
            this.btnDigit9.Location = new System.Drawing.Point(88, 114);
            this.btnDigit9.Name = "btnDigit9";
            this.btnDigit9.Size = new System.Drawing.Size(32, 32);
            this.btnDigit9.TabIndex = 16;
            this.btnDigit9.Text = "9";
            this.btnDigit9.UseVisualStyleBackColor = true;
            this.btnDigit9.Click += new System.EventHandler(this.btnDigit_Click);
            // 
            // btnDigit8
            // 
            this.btnDigit8.Location = new System.Drawing.Point(50, 114);
            this.btnDigit8.Name = "btnDigit8";
            this.btnDigit8.Size = new System.Drawing.Size(32, 32);
            this.btnDigit8.TabIndex = 15;
            this.btnDigit8.Text = "8";
            this.btnDigit8.UseVisualStyleBackColor = true;
            this.btnDigit8.Click += new System.EventHandler(this.btnDigit_Click);
            // 
            // btnDigit7
            // 
            this.btnDigit7.Location = new System.Drawing.Point(12, 114);
            this.btnDigit7.Name = "btnDigit7";
            this.btnDigit7.Size = new System.Drawing.Size(32, 32);
            this.btnDigit7.TabIndex = 14;
            this.btnDigit7.Text = "7";
            this.btnDigit7.UseVisualStyleBackColor = true;
            this.btnDigit7.Click += new System.EventHandler(this.btnDigit_Click);
            // 
            // btnDigit0
            // 
            this.btnDigit0.Location = new System.Drawing.Point(50, 228);
            this.btnDigit0.Name = "btnDigit0";
            this.btnDigit0.Size = new System.Drawing.Size(32, 32);
            this.btnDigit0.TabIndex = 17;
            this.btnDigit0.Text = "0";
            this.btnDigit0.UseVisualStyleBackColor = true;
            this.btnDigit0.Click += new System.EventHandler(this.btnDigit_Click);
            // 
            // btnNegate
            // 
            this.btnNegate.Location = new System.Drawing.Point(12, 228);
            this.btnNegate.Name = "btnNegate";
            this.btnNegate.Size = new System.Drawing.Size(32, 32);
            this.btnNegate.TabIndex = 18;
            this.btnNegate.Text = "±";
            this.btnNegate.UseVisualStyleBackColor = true;
            this.btnNegate.Click += new System.EventHandler(this.btnNegate_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(12, 76);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(70, 32);
            this.btnClear.TabIndex = 19;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnBackspace
            // 
            this.btnBackspace.Location = new System.Drawing.Point(88, 76);
            this.btnBackspace.Name = "btnBackspace";
            this.btnBackspace.Size = new System.Drawing.Size(32, 32);
            this.btnBackspace.TabIndex = 20;
            this.btnBackspace.Text = "⌫";
            this.btnBackspace.UseVisualStyleBackColor = true;
            this.btnBackspace.Click += new System.EventHandler(this.btnBackspace_Click);
            // 
            // btnDecimal
            // 
            this.btnDecimal.Location = new System.Drawing.Point(88, 228);
            this.btnDecimal.Name = "btnDecimal";
            this.btnDecimal.Size = new System.Drawing.Size(32, 32);
            this.btnDecimal.TabIndex = 21;
            this.btnDecimal.Text = ".";
            this.btnDecimal.UseVisualStyleBackColor = true;
            this.btnDecimal.Click += new System.EventHandler(this.btnDigit_Click);
            // 
            // textDisplay
            // 
            this.textDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textDisplay.Location = new System.Drawing.Point(12, 38);
            this.textDisplay.Name = "textDisplay";
            this.textDisplay.ReadOnly = true;
            this.textDisplay.Size = new System.Drawing.Size(146, 32);
            this.textDisplay.TabIndex = 22;
            this.textDisplay.Text = "input";
            this.textDisplay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblDisplay
            // 
            this.lblDisplay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDisplay.AutoSize = true;
            this.lblDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDisplay.Location = new System.Drawing.Point(12, 9);
            this.lblDisplay.Name = "lblDisplay";
            this.lblDisplay.Size = new System.Drawing.Size(48, 20);
            this.lblDisplay.TabIndex = 23;
            this.lblDisplay.Text = "result";
            this.lblDisplay.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(170, 274);
            this.Controls.Add(this.lblDisplay);
            this.Controls.Add(this.textDisplay);
            this.Controls.Add(this.btnDecimal);
            this.Controls.Add(this.btnBackspace);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnNegate);
            this.Controls.Add(this.btnDigit0);
            this.Controls.Add(this.btnDigit9);
            this.Controls.Add(this.btnDigit8);
            this.Controls.Add(this.btnDigit7);
            this.Controls.Add(this.btnDigit6);
            this.Controls.Add(this.btnDigit5);
            this.Controls.Add(this.btnDigit4);
            this.Controls.Add(this.btnDigit3);
            this.Controls.Add(this.btnDigit2);
            this.Controls.Add(this.btnDigit1);
            this.Controls.Add(this.btnCalc);
            this.Controls.Add(this.btnOpDiv);
            this.Controls.Add(this.btnOpMul);
            this.Controls.Add(this.btnOpMin);
            this.Controls.Add(this.btnOpAdd);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form3";
            this.Text = "LolCalc";
            this.Load += new System.EventHandler(this.Form3_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnOpMin;
        private System.Windows.Forms.Button btnOpMul;
        private System.Windows.Forms.Button btnOpDiv;
        private System.Windows.Forms.Button btnCalc;
        private System.Windows.Forms.Button btnOpAdd;
        private System.Windows.Forms.Button btnDigit1;
        private System.Windows.Forms.Button btnDigit2;
        private System.Windows.Forms.Button btnDigit3;
        private System.Windows.Forms.Button btnDigit6;
        private System.Windows.Forms.Button btnDigit5;
        private System.Windows.Forms.Button btnDigit4;
        private System.Windows.Forms.Button btnDigit9;
        private System.Windows.Forms.Button btnDigit8;
        private System.Windows.Forms.Button btnDigit7;
        private System.Windows.Forms.Button btnDigit0;
        private System.Windows.Forms.Button btnNegate;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnBackspace;
        private System.Windows.Forms.Button btnDecimal;
        private System.Windows.Forms.TextBox textDisplay;
        private System.Windows.Forms.Label lblDisplay;
    }
}

