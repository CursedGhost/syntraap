﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Temperature_Converter
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		// When one of the TextBoxes is changed, the other is programmatically updated.
		// This will trigger the other TextBox's TextChanged event which will try to change the original TextBox...
		// To stop this infinite loop of events, temporarily disable changing the other TextBox.
		bool changingTheText = false;

		public MainWindow()
		{
			InitializeComponent();
		}

		private void tboxCelsius_TextChanged(object sender, TextChangedEventArgs e)
		{
			// Only update when the user is changing us
			if (!changingTheText) {
				// Disable automatic updating of the other TextBox
				changingTheText = true;

				// Change the other TextBox to a neutral white
				tboxFahrenheit.Background = Brushes.White;

				// Try parsing the input as a double precision number
				double c;
				if (double.TryParse(tboxCelsius.Text, out c)) {
					// Convert from Celsius to Fahrenheit
					double f = c * (9.0 / 5.0) + 32.0;
					
					// Display the result, here string.Format is used to control the precision (2 digits after the comma).
					// Also acceptable would be a simple f.ToString();
					tboxFahrenheit.Text = string.Format("{0:0.00}", f);

					// Set the background as you're writing a valid number
					tboxCelsius.Background = Brushes.LightGreen;
				}
				else {
					// User did not type a valid number, set the background to redish color
					tboxCelsius.Background = Brushes.LightSalmon;
				}
				// Enable automatic updating of the other TextBox
				changingTheText = false;
			}
		}

		private void tboxFahrenheit_TextChanged(object sender, TextChangedEventArgs e)
		{
			// See above for detailed comments, this is much the same with some details changed
			if (!changingTheText) {
				changingTheText = true;
				tboxCelsius.Background = Brushes.White;
				double f;
				if (double.TryParse(tboxFahrenheit.Text, out f)) {
					double c = (f - 32.0) * (5.0 / 9.0);
					tboxCelsius.Text = string.Format("{0:0.00}", c);
					tboxFahrenheit.Background = Brushes.LightGreen;
				}
				else {
					tboxFahrenheit.Background = Brushes.LightSalmon;
				}
				changingTheText = false;
			}
		}
	}
}
