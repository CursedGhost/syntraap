﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Flight_Booker
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();

			string s = DateTime.Now.ToShortDateString();
			tboxStartDate.Text = s;
			tboxReturnDate.Text = s;
		}

		private void cboxSelect_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (tboxReturnDate!=null) {
				Validate();
			}
		}

		private void Validate()
		{
			tboxReturnDate.IsEnabled = cboxSelect.SelectedIndex != 0;

			bool valid = true;
			DateTime dateStart, dateReturn;

			// Validate Start Date
			if (!DateTime.TryParse(tboxStartDate.Text, out dateStart)) {
				tboxStartDate.Background = Brushes.LightSalmon;
				valid = false;
			}
			else {
				tboxStartDate.Background = Brushes.White;
			}

			// Validate Return Date
			if (!DateTime.TryParse(tboxReturnDate.Text, out dateReturn)) {
				tboxReturnDate.Background = Brushes.LightSalmon;
				valid = false;
			}
			else {
				tboxReturnDate.Background = Brushes.White;
			}

			// Start Date must be earlier than Return Date!
			if (valid && dateStart > dateReturn) {
				valid = false;
			}

			btnBook.IsEnabled = valid;
		}

		private void btnBook_Click(object sender, RoutedEventArgs e)
		{
			if (cboxSelect.SelectedIndex == 0) {
				MessageBox.Show("You have booked a one-way flight on " + tboxStartDate.Text + ".", "Flight Booked!");
			}
			else if (cboxSelect.SelectedIndex == 1) {
				MessageBox.Show("You have booked a flight on " + tboxStartDate.Text + " and will return on " + tboxReturnDate.Text + ".", "Flight Booked!");
			}
		}

		private void TextChanged(object sender, TextChangedEventArgs e)
		{
			Validate();
		}
	}
}
