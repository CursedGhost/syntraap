﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Counter
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
		}

		private void btnClicker_Click(object sender, RoutedEventArgs e)
		{
			int value;
			if (int.TryParse(tboxDisplay.Text, out value)) {
				value += 1;
				tboxDisplay.Text = value.ToString();
			}
			else {
				tboxDisplay.Text = "0";
			}
		}
	}
}
